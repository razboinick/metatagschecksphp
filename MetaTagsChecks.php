<?php
$tagsExpectedFile = 'testData.csv';
$tagsExpected = null;
$sourceInfo = null;

$errors = array();
$fileOutput = 'log.txt';

mainFunction();

/**
  * Send request and check the changes made in a proper way
  */
function mainFunction()
{
    global $sourceInfo;
    readCsvSource();

    //Go through the list of all web pages got from csv and check tags
    foreach ($sourceInfo as $sourceItem) {
        try {
            $ch = curl_init();
            //Set the URL that I would like to GET.
            curl_setopt($ch, CURLOPT_URL, $sourceItem['Url']);
            //Set up the cookies to get META tags due to task
            curl_setopt($ch, CURLOPT_COOKIE, "test='seo'");
            //Set CURLOPT_RETURNTRANSFER so that the content is returned as a variable.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            //Set CURLOPT_FOLLOWLOCATION to true to follow redirects.
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            //Execute the request.
            $data = curl_exec($ch);
            // Close curl handle
            curl_close($ch);

            if ($data === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            //Extract and check the title tag
            $title = preg_match('/<title[^>]*>(.*?)<\/title>/ims', $data, $matches) ? $matches[1] : null;

            checkTitle($sourceItem['Url'],$sourceItem['Title'],$title);

            //Parsing response to get value of meta description tag
            $doc = new DOMDocument();
            //This is needed for correct Cyrillic parsing
            @$doc->loadHTML('<?xml encoding="utf-8" ?>' . $data);
            $metaTags = $doc->getElementsByTagName('meta');
            foreach ($metaTags as $metaTag) {
                if ($metaTag->getAttribute('name') == 'description') {

                    //Checking meta description tag
                    checkDescription($sourceItem['Url'],$sourceItem['Meta Description'],$metaTag->getAttribute('content'));
                    break;
                }
            }

        } catch (Exception $e) {
            trigger_error(sprintf(
                'Curl failed with error #%d: %s',
                $e->getCode(), $e->getMessage()),
                E_USER_ERROR);
        }
    }

    //Check errors existence and generate output file
    createOutput();
}

/**
 * Get URLs and expected tags from csv source
 */
function readCsvSource()
{
    global $tagsExpectedFile;
    global $sourceInfo;

    //Reading csv file with header
    $sourceInfo = array_map("str_getcsv", file($tagsExpectedFile, FILE_SKIP_EMPTY_LINES));
    $keys = array_shift($sourceInfo);

    //Creating nice associative array
    foreach ($sourceInfo as $i => $row) {
        $sourceInfo[$i] = array_combine($keys, $row);
    }
}

/**
 * Check title and in case of bug add message to errors array
 * @param $url
 * @param $expected
 * @param $observed
 *
 */
function checkTitle($url,$expected,$observed)
{
    global $errors;
    if($expected!=$observed)
    {
        //Adding message to errors array
        array_push($errors,'Web page "'.$url.'" has discrepancy in expected title:'.PHP_EOL.$expected.PHP_EOL.'and observed title:'.PHP_EOL.$observed.PHP_EOL.PHP_EOL);
    }
}

/**
 * Check meta description and in case of bug add message to errors array
 * @param $url
 * @param $expected
 * @param $observed
 */
function checkDescription($url,$expected,$observed)
{
    global $errors;
    if($expected!=$observed)
    {
        //Adding message to errors array
        array_push($errors,'Web page "'.$url.'" has discrepancy in expected meta description:'.PHP_EOL.$expected.PHP_EOL.'and observed meta description:'.PHP_EOL.$observed.PHP_EOL.PHP_EOL);
    }
}

/**
 * In case of errors output message and create file with detailed info
 */
function createOutput()
{
    global $errors;
    global $fileOutput;

    if ($errors != null && count($errors)>0)
    {
        echo ('There are some discrepancies, see more in "'.$fileOutput.'"');

        for ($i = 0; $i < count($errors); $i++) {
            //Rewrite output file for every new script run
            if($i == 0){
                file_put_contents($fileOutput,$errors[$i]);
            }
            else{
                //Add errors to file
                file_put_contents($fileOutput,$errors[$i],FILE_APPEND);
            }
        }
    }
    else {
        echo('Everything is correct!');
    }
}